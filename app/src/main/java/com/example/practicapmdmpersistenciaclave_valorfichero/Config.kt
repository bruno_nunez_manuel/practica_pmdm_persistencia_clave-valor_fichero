package com.example.practicapmdmpersistenciaclave_valorfichero

import android.content.Context
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener


/**@author BRUNO NÚÑEZ MANUEL   1º DAM DUAL B   12/03/2021**/
class Config : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_config)

        /*Soy un rebelde y no voy a cumplir con los requisitos especificados en la tarea (me
        gusta el riesgo). En vez de usar un botón para guardar los campos, lo voy a hacer en
        "tiempo real" en función de si cambia el texto de los editText de username y password.
        Me ha parecido más guay aunque a lo mejor hay gente incapaz de inferir el funcionamiento
        de semejante obra de ingeniería alienígena. Espero que no me pongas un 0 por esto :)
        Añadiré un Toast para que el usuario no sufra una crisis existencial...*/

        // Al iniciar Activity, setea Switch (ON/OFF) en función de si hay password en shared_pref.
        setSwitchState()

        // Lo que se escriba en Username, será el UserName: en Tiempo Real.
        userNameSync()

        // Lo que se escriba en Nueva Contraseña, será el password: en Tiempo Real.
        // Nótese que la contraseña actual se vuelve visible si se intenta cambiarla.
        passwordSync()

        // Por comodidad (para mí), cualquier cambio de estado del Switch destruye la constraseña.
        resetBySwitch()
    }

    private fun setSwitchState() {
        val seguridadLinearLayout = findViewById<LinearLayout>(R.id.seguridadLinearLayout)
        val seguridadSwitch = findViewById<Switch>(R.id.seguridadSwitch)
        val contrasenaActualTextView = findViewById<TextView>(R.id.contrasenaActualTextView)
        val showPasswordTextView = findViewById<TextView>(R.id.showPasswordTextView)
        val sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE)

        seguridadLinearLayout.isVisible = false
        contrasenaActualTextView.isVisible = false
        showPasswordTextView.isVisible = false
        if (sharedPreferences.getString("password", "").isNullOrBlank()) {
            seguridadSwitch.isChecked = false
        } else {
            seguridadSwitch.isChecked = true
            seguridadLinearLayout.isVisible = true
        }
    }

    private fun userNameSync() {
        val usernameModificarEditText = findViewById<EditText>(R.id.usernameModificarEditText)
        val sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        usernameModificarEditText.setText(
            sharedPreferences
                .getString("username", "Sin Nombre").toString()
        )
        usernameModificarEditText.addTextChangedListener {
            editor.putString("username", usernameModificarEditText.text.toString())
            editor.apply()
            Toast.makeText(
                    this, "Nombre de Usuario actualizado!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun passwordSync() {
        val contrasenaActualTextView = findViewById<TextView>(R.id.contrasenaActualTextView)
        val showPasswordTextView = findViewById<TextView>(R.id.showPasswordTextView)
        val nuevoPasswordEditText = findViewById<EditText>(R.id.nuevoPasswordEditText)
        val sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        nuevoPasswordEditText.addTextChangedListener {
            editor.putString("password", nuevoPasswordEditText.text.toString())
            editor.apply()
            Toast.makeText(this, "Contraseña Actualizada!", Toast.LENGTH_SHORT).show()
        }
        nuevoPasswordEditText.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                showPasswordTextView.text =
                    sharedPreferences.getString("password", "(Sin Contraseña)")
                contrasenaActualTextView.isVisible = true
                showPasswordTextView.isVisible = true
            }
        }
    }

    private fun resetBySwitch() {
        val seguridadLinearLayout = findViewById<LinearLayout>(R.id.seguridadLinearLayout)
        val seguridadSwitch = findViewById<Switch>(R.id.seguridadSwitch)
        val showPasswordTextView = findViewById<TextView>(R.id.showPasswordTextView)
        val sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        seguridadSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                seguridadLinearLayout.isVisible = true
                showPasswordTextView
                    .setText(sharedPreferences.getString("password",
                        "(Sin Contraseña)")).toString()
            } else {
                editor.remove("password")
                editor.apply()
                seguridadLinearLayout.isVisible = false
            }
        }
    }

}
