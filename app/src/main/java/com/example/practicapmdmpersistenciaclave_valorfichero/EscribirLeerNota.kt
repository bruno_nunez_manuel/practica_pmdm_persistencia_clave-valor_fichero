package com.example.practicapmdmpersistenciaclave_valorfichero

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.io.BufferedReader
import java.io.FileNotFoundException
import java.io.InputStreamReader


/**@author BRUNO NÚÑEZ MANUEL   1º DAM DUAL B   12/03/2021**/
class EscribirLeerNota : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_escribir_leer_nota)

        val notaSecretaEditText = findViewById<EditText>(R.id.notaSecretaEditText)
        val guardarButton = findViewById<Button>(R.id.guardarButton)
        val recuperarButton = findViewById<Button>(R.id.recuperarButton)
        val configButton = findViewById<Button>(R.id.configButton)

        guardarButton.setOnClickListener {
            if (notaSecretaEditText.text.isNotBlank()) {
                val outputStream = openFileOutput("fichero.txt", Context.MODE_PRIVATE)
                outputStream.write(notaSecretaEditText.text.toString().toByteArray())
                outputStream.close()
                notaSecretaEditText.setText("")
                Toast.makeText(this, "Nota Guardada!", Toast.LENGTH_SHORT)
                        .show()
            } else {
                Toast.makeText(this, "Escribe algo en la nota...", Toast.LENGTH_SHORT)
                        .show()
            }
        }

        recuperarButton.setOnClickListener {
            try {
                val inputStream = openFileInput("fichero.txt")
                val inputStreamReader = InputStreamReader(inputStream)
                val buffer = BufferedReader(inputStreamReader)
                val string = buffer.readLine()
                if (string.isNullOrBlank()) {
                    Toast.makeText(this, "Nota Secreta vacía...", Toast.LENGTH_SHORT)
                            .show()
                }
                notaSecretaEditText.setText(string)
                inputStream.close()
            } catch (e: FileNotFoundException) {
                Toast.makeText(this, "Nota Secreta vacía...", Toast.LENGTH_SHORT)
                        .show()
            }
        }

        configButton.setOnClickListener {
            Intent(this, Config::class.java).also {
                startActivity(it)
            }
        }
    }

    override fun onBackPressed() {
        val sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE)
        if (sharedPreferences.getString("password", "").isNullOrBlank()) {
            Toast.makeText(
                    this, "Activa Seguridad y crea una contraseña en Configuración " +
                    "para acceder a la pantalla de login...", Toast.LENGTH_LONG).show()
        } else {
            super.onBackPressed()
        }
    }

    override fun onRestart() {
        super.onRestart()
        val notaSecretaEditText = findViewById<EditText>(R.id.notaSecretaEditText)
        notaSecretaEditText.text = null // Ni el pentágono tiene tanta seguridad.
    }

}