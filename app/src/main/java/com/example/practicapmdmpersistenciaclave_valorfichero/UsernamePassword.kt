package com.example.practicapmdmpersistenciaclave_valorfichero

import android.app.UiModeManager.MODE_NIGHT_YES
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate


/**@author BRUNO NÚÑEZ MANUEL   1º DAM DUAL B   12/03/2021**/
class UsernamePassword : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // He intentado hacerlo en el manifest pero no funciona en versiones arcaicas.
        AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES)

        val usernameEditText = findViewById<EditText>(R.id.usernameEditText)
        val passwordEditText = findViewById<EditText>(R.id.passwordEditText)
        val entrarButton = findViewById<Button>(R.id.entrarButton)
        val sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE)

        usernameEditText
                .setText(sharedPreferences.getString("username",  "Sin Nombre"))

        if (sharedPreferences.getString("password", "").isNullOrBlank()) {
            Intent(this, EscribirLeerNota::class.java).also {
                startActivity(it)
            }
        }

        // Esto lo habría metido en un else si no quedase estéticamente inasumible.
        entrarButton.setOnClickListener {
            if (passwordEditText.text.toString() ==
                sharedPreferences.getString("password", "").toString()
            ) {
                Intent(this, EscribirLeerNota::class.java).also {
                    startActivity(it)
                }
            } else {
                Toast.makeText(this, "Constraseña Incorrecta", Toast.LENGTH_SHORT)
                        .show()
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        val sharedPreferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE)
        val usernameEditText = findViewById<EditText>(R.id.usernameEditText)
        val passwordEditText = findViewById<EditText>(R.id.passwordEditText)

        usernameEditText
                .setText(sharedPreferences.getString("username", "(Sin nombre)"))

        passwordEditText.text = null // Por seguridad, no vaya a ser...
    }

}